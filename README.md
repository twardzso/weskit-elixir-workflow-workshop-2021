# weskit-elixir-workflow-workshop-2021

This a small tutorial for sending workflows to WESkit.

# Initalization

You can clone the tutorial repository from you cloud VM or on your local machine.

1. Clone repository

```bash
cd $HOME
git clone https://gitlab.com/twardzso/weskit-elixir-workflow-workshop-2021.git
cd weskit-elixir-workflow-workshop-2021
```

2. On Theia VM, we need to activate conda

```bash
conda init bash
source ~/.bashrc
```

3. and then install the conda environment.

```bash
conda env create --file environment.yaml --name weskit_demo
conda activate weskit_demo
```

# Send Workflow to WESkit

* Next, we will submit a workflow to the WESkit instance at weskit.bihealth.org using python
* Therefore, we start python, load requried modules and set a server URL variable

```python
import requests, json, yaml, pprint, os
pp = pprint.PrettyPrinter(indent=2)

WES_URL="https://weskit.bihealth.org"
```

* We can get general information about the WESkit instace by calling the service-info endpoint

```python
info = requests.get("{}/ga4gh/wes/v1/service-info".format(WES_URL), verify=False)
pp.pprint(info.json())
```

* Next we create a function to load the metadata that is required for submitting a workflow. In this case we will submit the execution of a Snakemake workflow.

```python

def create_data():
    with open(os.path.normpath("config.yaml")) as file:
        workflow_params = yaml.load(file, Loader=yaml.FullLoader)
    data = {"workflow_params": workflow_params,
            "workflow_type": "snakemake",
            "workflow_type_version": "5.8.2",
            "workflow_url": "file:tests/genome_analysis/Snakefile"} # path of workflow file is defined at the WESkit instance
    return(data)

```

* For authentification, we need to create a header object which contains an access token. Here, the access token is requested from a Keycloak instance running on the same weskit cluster.

```python

def create_header():
  keycloak_host = "https://weskit.bihealth.org/auth/realms/WESkit/protocol/openid-connect/token"
  credentials = dict(username="test",
                    password="test",
                    client_id="OTP",
                    client_secret="7670fd00-9318-44c2-bda3-1a1d2743492d",
                    grant_type="password")
  token = requests.post(url=keycloak_host, data=credentials, verify=False).json()
  header = dict(Authorization="Bearer " + token["access_token"])
  return(header)

```

* With the meta-data and the header, we can send a run to the WESkit instance:

```python
send_run = requests.post("%s/ga4gh/wes/v1/runs" % (WES_URL), json=create_data(), verify=False, headers=create_header())
pp.pprint(send_run.json())
```

* List all runs (from the OTP user):

```python
get_runs = requests.get("%s/ga4gh/wes/v1/runs" % (WES_URL), verify=False, headers=create_header())
pp.pprint(get_runs.json())
```

* Get information about single run:

```python
results = requests.get("{}/ga4gh/wes/v1/runs/{}".format(WES_URL, send_run.json()["run_id"]), verify=False, headers=create_header())
pp.pprint(results.json())
```
